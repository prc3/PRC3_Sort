CFLAGS=-Wall -Werror -pedantic -O3 -Icode -c
LDDFLAGS=-lpthread

MAIN_SOURCES=$(wildcard $(CLION_EXE_DIR)code/*.cpp)
MAIN_OBJECTS=$(MAIN_SOURCES:.cpp=.o) \
			 $(OBJECT_DIR)/FileStructure.o \
			 $(OBJECT_DIR)/Dir.o \
			 $(OBJECT_DIR)/File.o

TEST_SOURCES=$(wildcard test/test.cpp code/*.cpp)
TEST_OBJECTS=$(TEST_SOURCES:.cpp=.o) \
			 $(OBJECT_DIR)/FileStructure.o \
			 $(OBJECT_DIR)/Dir.o \
			 $(OBJECT_DIR)/File.o

TEST_LIBS=-lgtest -lgtest_main -lpthread

CC=g++ -ggdb

.phony: all clean


sort: $(MAIN_OBJECTS) Makefile code/FileStructure.h
	@$(CC) $(MAIN_OBJECTS) -ggdb -o $@ $(LDDFLAGS)

all: sort

.cpp.o: Makefile
	$(CC) $(CFLAGS) $< -o $@

clean:
	@rm -rf sort code/*.o test/*.o *.bin
