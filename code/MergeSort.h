#ifndef PRC3_SORT_MERGESORT_H
#define PRC3_SORT_MERGESORT_H

#include "FileStructure.h"

template <typename T>
class MergeSort {
public:
    void Start(T** head);
private:
    void Sort(T** headReference);
    T* Merge(T* firstHalfList, T* secondHalfList);
    void Split(T* unSplittedList, T** firstHalfList, T** secondHalfList);
};

template <typename T>
void MergeSort<T>::Start(T** head) {
     Sort(head);
}

template <typename T>
void MergeSort<T>::Sort(T** headReference) {
    T* head = *headReference;

    // The two splitted list halfs.
    T* firstHalfList;
    T* secondHalfList;

    // When there is no head or there is no getNext, we assume
    // we have max 1 item in the given list. Sorting is done.
    if(head == NULL || head->getNext() == NULL)
        return;

    // Split the list
    Split(head, &firstHalfList, &secondHalfList);

    // Sort the two halfs
    Sort(&firstHalfList);
    Sort(&secondHalfList);

    // Merge the two lists back together
    *headReference = Merge(firstHalfList, secondHalfList);
}

template <typename T>
T* MergeSort<T>::Merge(T* firstHalfList, T* secondHalfList) {
    // Pointer to the (now empty) merged list.
    T* mergedList = NULL;

    // If one of the two halfs is empty, we can return the other half.
    if(firstHalfList == NULL) return secondHalfList;
    if(secondHalfList == NULL) return firstHalfList;

    // Pick the lowest value and sort that one
    if(firstHalfList->getName().compare(secondHalfList->getName()) <= 0)
    {
        mergedList = firstHalfList;
        mergedList->setNext(Merge(firstHalfList->getNext(), secondHalfList));
    }
    else
    {
        mergedList = secondHalfList;
        mergedList->setNext(Merge(firstHalfList, secondHalfList->getNext()));
    }

    return (mergedList);
}

// Split list into two halves. Many thanks to Nick Parlante (http://cslibrary.stanford.edu/105/LinkedListProblems.pdf, page 15)
template <typename T>
void MergeSort<T>::Split(T* unSplittedList, T** firstHalfList, T** secondHalfList) {

    // Special case: less then two items in the list
    if(unSplittedList == NULL || unSplittedList->getNext() == NULL)
    {
        // Bring everything to the first half. Second half keeps empty.
        *firstHalfList = unSplittedList;
        *secondHalfList = NULL;
    }
    else
    {
        T* allItem = unSplittedList;
        T* oddItem = unSplittedList->getNext();

        while(oddItem != NULL)
        {
            oddItem = oddItem->getNext();
            if(oddItem != NULL)
            {
                allItem = allItem->getNext();
                oddItem = oddItem->getNext();
            }
        }

        // When oddItem is finished, allItem is one item before the midpoint
        // of the list.
        // If we have an even list, the midpoint is the item before the midpoint.

        *firstHalfList = unSplittedList;
        *secondHalfList = allItem->getNext(); // Add one, the absolute midpoint.
        allItem->setNext(NULL); // Break the first half
    }
}



#endif //PRC3_SORT_MERGESORT_H
