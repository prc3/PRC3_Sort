#include "FileStructure.h"
#include "PathTraveler.h"
#include <iostream>

int main()
{
    FileStructure f;
    Dir head;
    
    f.loadFile("data/gibberish.bin", head);

    PathTraveler pathTraveler;

    Dir* headDir = &head;
    pathTraveler.Sort(headDir);
    head = *headDir;

    f.saveFile(head, "sorted.bin");

    return 0;
}
