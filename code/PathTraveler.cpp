#include "PathTraveler.h"


void PathTraveler::SortDir(Dir** currentDirectory)
{
    Dir* head = *currentDirectory;

    Dir* subDir = head->getFirstSubdir();
    {
        MergeSort <Dir> sorter;
        sorter.Start(&subDir);
        head->setFirstSubdir(subDir);
    }

    File* files = head->getFirstFile();
    {
        MergeSort <File> sorter;
        sorter.Start(&files);
        head->setFirstFile(files);
    }

}

void PathTraveler::Sort(Dir*& currentDirectory)
{
    SortDir(&currentDirectory);

    Dir* subDir = currentDirectory->getFirstSubdir();
    while(subDir != NULL)
    {
        Sort(subDir);å
        subDir = subDir->getNext();
    }

}
