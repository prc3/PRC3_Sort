#ifndef PRC3_SORT_PATHTRAVELER_H
#define PRC3_SORT_PATHTRAVELER_H

#include "FileStructure.h"
#include "MergeSort.h"

class PathTraveler {
private:

public:
    void SortDir(Dir** currentDirectory);
    void Sort(Dir*& currentDirectory);
};

#endif //PRC3_SORT_PATHTRAVELER_H
